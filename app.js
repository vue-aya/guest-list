new Vue({
    el: '#app',
    data: {
        event: {
            date: 'August 14th - 16th',
            title: 'Summer <i>Festival!</i>',
            text: 'It\'s back! This years summer festival will be in the beautiful countryside featuring our best line up ever!',
            text2: 'Add your name to the guest list for exclusive offers:'
        },
        newNameText: '',
        guestNames: [],
        appStyles: {
            marginTop: '25px'
        },
        eventCapacity: 25,
        eventCapacityPercentage: 0,
    },
    methods: {
        formSubmitted: function () {
            console.log("ddddd");
            if (this.newNameText.length > 0 && this.eventCapacityPercentage < 100) {
                this.guestNames.push(this.newNameText);
                this.newNameText = '';
                this.eventCapacityPercentage = this.guestNames.length / (this.eventCapacity / 100);
            }
        },
        keyPressed: function () {
            console.log("space pressed");
        }
    },
    computed: {
        sortNames: function () {
            return this.guestNames.sort();
        }
    },
    watch: {
        guestNames: function (data) {
            console.log("watch triggered");
        }
    },
    filters: {
        formatName: function (value) {
            return value.slice(0, 1).toUpperCase() + value.slice(1).toLowerCase();
        }
    }
});

new Vue({
    el: "#navigation",
    data: {
        appName: 'Guest List',
        navLinks: [
            {name: "Home", id: 1,url:"https://www.google.com"},
            {name: "Upcoming Events", id: 2,url:"https://www.google.com"},
            {name: "Guest Benefits", id: 3,url:"https://www.google.com"},
            {name: "Latest News", id: 4,url:"https://www.google.com"}
        ],
        
    },
    methods:{
        titleChange:function(){
            this.$refs.name.innerText="TitleChanged";
        }
    }
});
vm3= new Vue({
   template:'<div><h1>Guest List'
           +'</h1></div>'
});
vm3.$mount("#navigation");